#Luckyman

This is a web game win a million. If you want to win money answer interesting questions!

### Installing



```
git clone https://gitlab.com/tkachukvladislav/lucky
```

And repeat

```
cd luckymen

composer install

php init 

php yii migrate 

```

End with an example of getting some data out of the system or using it for a little demo

## Deployment


What would go into the admin panel on the test user.

login = admin
password = 251

## Built With

* [Yii2](https://www.yiiframework.com/) - The web framework used
* [composer](https://getcomposer.org/) - Dependency Management
* [PostgreSQL](https://www.postgresql.org/) - Dependency Management



## Authors

* **Tkachuk Vladislav** - *Initial work* - [TkachukVladislav](https://gitlab.com/tkachukvladislav)
* **Sydor Petro** - *Initial work* - [PetroSydor](https://gitlab.com/sydor.pete)

See also the list of [Lucky](https://gitlab.com/tkachukvladislav/lucky) who participated in this project.
!