<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property string $id
 * @property string $question
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $answer
 * @property int $level
 * @property string|null $create_date
 * @property string|null $update_date
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'question', 'a', 'b', 'c', 'd', 'answer', 'level'], 'required'],
            [['id'], 'string'],
            [['level'], 'default', 'value' => null],
            [['level'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['question', 'a', 'b', 'c', 'd', 'answer'], 'string', 'max' => 255],
            [['question'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'a' => Yii::t('app', 'A'),
            'b' => Yii::t('app', 'B'),
            'c' => Yii::t('app', 'C'),
            'd' => Yii::t('app', 'D'),
            'answer' => Yii::t('app', 'Answer'),
            'level' => Yii::t('app', 'Level'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_date' => Yii::t('app', 'Update Date'),
        ];
    }
}
