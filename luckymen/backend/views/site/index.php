<?php

/* @var $this yii\web\View */

$this->title = 'О Счатсливчик';

use yii\helpers\Url; ?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">This admin panel lucky game!!</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Table Question</h2>

                <p>This button edit question </p>

                <p><a class="btn btn-lg btn-success" href=<?=Url::to('question/index')?>>Edit</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Table User</h2>

                <p>This button edit user</p>

                <p><a class="btn btn-lg btn-success" href=<?= Url::to('user/index')?>>Edit</a></p>
            </div>

        </div>

    </div>
</div>
