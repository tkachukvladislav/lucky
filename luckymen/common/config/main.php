<?php


use frontend\repositories\QuestionRepositories;
use frontend\service\QuestionService;
use frontend\repositories\UserRepositories;
use frontend\service\UserService;
use frontend\service\StatisticService;
use frontend\repositories\StatisticRepositories;
use yii\di\Instance;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],'container' => [
        'definitions' => [
            'yii\widgets\LinkPager' => ['maxButtonCount' => 5]
        ],
        'singletons' => [
            UserRepositories::class,
            UserService::class => [
                [],
                [
                    Instance::of(UserRepositories::class),
                ],
            ],
            QuestionRepositories::class,
            QuestionService::class => [
                [],
                [
                    Instance::of(QuestionRepositories::class),
                ],
            ],
            StatisticRepositories::class,
            StatisticService::class => [
                [],
                [
                    Instance::of(StatisticRepositories::class),
                ],
            ],


        ],
    ],

];
