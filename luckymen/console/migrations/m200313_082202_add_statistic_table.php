<?php

use console\uuidTrait\UuidTypeTrait;
use yii\db\Migration;

/**
 * Class m200313_082202_add_statistic_table
 */
class m200313_082202_add_statistic_table extends Migration
{   use UuidTypeTrait;
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%statistic}}', [
            'id' => $this->uuid(),
            'result' => $this->bigInteger()->notNull(),
            'date_game' => $this->timestamp()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('primary_id_statistic', 'statistic', 'id');
        $this->addForeignKey('FK_id_statistic_id_user', 'statistic', 'id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%statistic}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200313_082202_add_statistic_table cannot be reverted.\n";

        return false;
    }
    */
}
