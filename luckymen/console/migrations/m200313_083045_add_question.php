<?php

use console\uuidTrait\UuidTypeTrait;
use yii\db\Migration;

/**
 * Class m200313_083045_add_question
 */
class m200313_083045_add_question extends Migration
{
    use UuidTypeTrait;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%question}}', [
            'id' => $this->uuid(),
            'question' => $this->string(255)->notNull()->unique(),
            'a' => $this->string(255)->notNull(),
            'b' => $this->string(255)->notNull(),
            'c' => $this->string(255)->notNull(),
            'd' => $this->string(255)->notNull(),
            'answer' => $this->string(255)->notNull(),
            'level' => $this->integer()->notNull(),
            'create_date' => $this->timestamp(),
            'update_date' => $this->timestamp(),

        ], $tableOptions);
        $this->addPrimaryKey('primary_id_qustion', 'question', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%question}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200313_083045_add_question cannot be reverted.\n";

        return false;
    }
    */
}
