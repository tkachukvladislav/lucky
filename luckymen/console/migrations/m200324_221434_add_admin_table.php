<?php

use console\uuidTrait\UuidTypeTrait;
use yii\db\Migration;

/**
 * Class m200324_221434_add_admin_table
 */
class m200324_221434_add_admin_table extends Migration
{
    use UuidTypeTrait;
    /**
     * Add table admin and
     * insert admin
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('admin', [
            'id' => $this->uuid(),
            'login' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

        ], $tableOptions);
        $this->addPrimaryKey('primary_id_admin', 'admin', 'id');
        $this->insert('admin', [
            'id'=>'9839a2fc-5802-4a07-8e13-ea2b2ccff3a3',
            'login'=>'admin',
            'auth_key'=>'1asd124',
            'password_hash'=>'$2y$13$UI6UvAlxa7ohR1466KzEN.ixgOZ2qtITTwf89WlCtOcainVIkpTwS',
            'email'=>'admin@mail.ru'
            ]);
    }

    /**
     * Drop table admin
     */
    public function safeDown()
    {
      $this->dropTable('admin');
    }


}
