<?php

use yii\db\Migration;

/**
 * Class m200325_151717_statistic_fix
 */
class m200325_151717_statistic_fix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->dropIndex('primary_id_statistic', 'statistic');
        $this->dropPrimaryKey('primary_id_statistic', 'statistic');
        $this->createIndex('primary_id_statistic', 'statistic', 'id', $unique = false );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addPrimaryKey('primary_id_statistic', 'statistic', 'id');
        $this->createIndex('primary_id_statistic', 'statistic', 'id', $unique = true );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200325_151717_statistic_fix cannot be reverted.\n";

        return false;
    }
    */
}
