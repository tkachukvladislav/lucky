<?php

use yii\db\Migration;

/**
 * Class m200325_153320_question_bootstrap
 */
class m200325_153320_question_bootstrap extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('question', [
            'id'=>'9839a2fc-5802-4a07-8e13-ea2b2ccff3a3',
            'question' => "Question 1 - 1 option B",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "B",
            'level' => "1",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'688b4a45-5922-4cff-96be-6f310bfa9403',
            'question' => "Question 1 - 2 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "1",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'2189da58-35ca-4ea8-a447-48be27e945f8',
            'question' => "Question 2 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "2",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'b7949eb7-3a53-46e3-9c8e-5910b65d4dbe',
            'question' => "Question 2 - 2 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "2",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'7beebe68-ba8f-417a-a5c3-4cb0775077ac',
            'question' => "Question 3 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "3",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'4e590656-f3e9-4a33-8fd7-895ad78ec681',
            'question' => "Question 3 - 2 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "3",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'f40ad0b4-7134-47a9-ac55-0c78375a6fc4',
            'question' => "Question 4 - 1 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "4",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'186bb7ff-ec85-49b9-bf0c-e5ca56b32d08',
            'question' => "Question 4 - 2 option D",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "D",
            'level' => "4",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'121eb661-b7e8-4d73-a979-c4be5d8ab6e7',
            'question' => "Question 5 - 1 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "5",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'26b5bde3-c997-4d59-991d-3fadbaa267a6',
            'question' => "Question 6 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "6",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'927b352e-8104-457d-8415-53ef6ae13e50',
            'question' => "Question 7 - 1 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "7",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'8bc44ef8-a577-4ca9-8194-223f2fbcc7ea',
            'question' => "Question 8 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "8",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'23c7dc64-d795-4934-8a24-9a1676bd01bc',
            'question' => "Question 9 - 1 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "9",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'f243d6a8-a5a8-4c53-be30-f8b43791c823',
            'question' => "Question 10 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "10",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'eeb39cc0-e009-4758-be5f-cb6bae8ae465',
            'question' => "Question 11 - 1 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "11",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'009b72a6-8499-49e5-b849-0660e44706e0',
            'question' => "Question 12 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "12",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'3afb4552-08ab-48fa-8e78-0f07f3537ef7',
            'question' => "Question 13 - 1 option C",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "C",
            'level' => "13",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'31c7224f-fe70-461f-a969-256b20a851ff',
            'question' => "Question 14 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "14",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);

        $this->insert('question', [
            'id'=>'7157f050-37b9-4de4-8955-93c98ea8b329',
            'question' => "Question 15 - 1 option A",
            'a' => "Answer A OPTS",
            'b' => "Answer B OPTS",
            'c' => "Answer C OPTS",
            'd' => "Answer F OPTS",
            'answer' => "A",
            'level' => "15",
            'create_date' => Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s')),
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('question', ['id'=>'9839a2fc-5802-4a07-8e13-ea2b2ccff3a3']);
        $this->delete('question', ['id'=>'688b4a45-5922-4cff-96be-6f310bfa9403']);
        $this->delete('question', ['id'=>'2189da58-35ca-4ea8-a447-48be27e945f8']);
        $this->delete('question', ['id'=>'b7949eb7-3a53-46e3-9c8e-5910b65d4dbe']);
        $this->delete('question', ['id'=>'7beebe68-ba8f-417a-a5c3-4cb0775077ac']);
        $this->delete('question', ['id'=>'4e590656-f3e9-4a33-8fd7-895ad78ec681']);
        $this->delete('question', ['id'=>'f40ad0b4-7134-47a9-ac55-0c78375a6fc4']);
        $this->delete('question', ['id'=>'186bb7ff-ec85-49b9-bf0c-e5ca56b32d08']);
        $this->delete('question', ['id'=>'121eb661-b7e8-4d73-a979-c4be5d8ab6e7']);
        $this->delete('question', ['id'=>'26b5bde3-c997-4d59-991d-3fadbaa267a6']);
        $this->delete('question', ['id'=>'927b352e-8104-457d-8415-53ef6ae13e50']);
        $this->delete('question', ['id'=>'8bc44ef8-a577-4ca9-8194-223f2fbcc7ea']);
        $this->delete('question', ['id'=>'23c7dc64-d795-4934-8a24-9a1676bd01bc']);
        $this->delete('question', ['id'=>'f243d6a8-a5a8-4c53-be30-f8b43791c823']);
        $this->delete('question', ['id'=>'eeb39cc0-e009-4758-be5f-cb6bae8ae465']);
        $this->delete('question', ['id'=>'009b72a6-8499-49e5-b849-0660e44706e0']);
        $this->delete('question', ['id'=>'3afb4552-08ab-48fa-8e78-0f07f3537ef7']);
        $this->delete('question', ['id'=>'31c7224f-fe70-461f-a969-256b20a851ff']);
        $this->delete('question', ['id'=>'7157f050-37b9-4de4-8955-93c98ea8b329']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200325_153320_question_bootstrap cannot be reverted.\n";

        return false;
    }
    */
}
