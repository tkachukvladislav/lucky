<?php

namespace console\uuidTrait;

use yii\base\NotSupportedException;
use yii\db\ColumnSchemaBuilder;
use yii\db\Connection;


trait UuidTypeTrait
{
    /**
     * @return Connection the databases connection to be used for schema building.
     */
    abstract protected function getDb();

    /**
     * @return ColumnSchemaBuilder
     *
     * @throws NotSupportedException
     */
    public function uuid()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('uuid');
    }
}