<?php


namespace frontend\assets;

use yii\web\AssetBundle;

class MillionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'million/css/style.css',
        'million/css/reset.css'
    ];
    public $js = [
        'million/js/main.js',
        'million/js/step.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}