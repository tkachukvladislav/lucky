<?php

namespace frontend\controllers;

use frontend\models\Question;
use frontend\form\ChangePasswordForm;
use frontend\form\LoginForm;
use frontend\form\RegistrationForm;
use frontend\models\Statistic;
use frontend\models\User;
use frontend\repositories\QuestionRepositories;
use frontend\service\StatisticService;
use frontend\service\UserService;
use frontend\service\QuestionService;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var QuestionService
     */
    private $questionService;
    /**
     * @var StatisticService
     */
    private $statisticService;
    /**
     * @var array
     */
    private array $gameMatch = [
        1 => 100,
        2 => 200,
        3 => 300,
        4 => 500,
        5 => 1000,
        6 => 2000,
        7 => 4000,
        8 => 8000,
        9 => 16000,
        10 => 32000,
        11 => 64000,
        12 => 125000,
        13 => 250000,
        14 => 500000,
        15 => 1000000
    ];

    /**
     * SiteController constructor.
     * @param $id
     * @param $module
     * @param array $config
     * @param UserService $userService
     */
    public function __construct(
        $id,
        $module,
        $config = [],
        UserService $userService,
        QuestionService $questionService,
        StatisticService $statisticService
    ) {
        $this->statisticService = $statisticService;
        $this->userService = $userService;
        $this->questionService = $questionService;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = false;
        $this->getView()->title = 'О счастливчик';
        $registrationForm = new RegistrationForm();
        $loginForm = new LoginForm();
        $changePasswordForm = new ChangePasswordForm();
        $session = Yii::$app->session;

        if ($registrationForm->load(Yii::$app->request->post()) && $registrationForm->validate()) {
            if ($this->userService->createUser($registrationForm)) {
                return $this->goHome();
            }
        }

        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->validate()) {
            if ($this->userService->loginUser($loginForm)) {
                return $this->goHome();
            }
        }
        if ($changePasswordForm->load(Yii::$app->request->post()) && $changePasswordForm->validate()) {
            $this->userService->changePasswordUser($changePasswordForm);
        }

//        if (!$session->isActive) {
//            $session->open();
//            $session->set('level', '1');
//            $session->set('user', Yii::$app->user->id);
//            $session->set('call', '0');
//            $session->set('hall', '0');
//            $session->set('fifty', '0');
//            $session->set('active', 'yes');
//        }

        if ((!$session->get('level')) ||  ($session->get('active') == 'no')){
            $session->open();
            $session->set('level', '1');
            $session->set('user', Yii::$app->user->id);
            $session->set('call', '0');
            $session->set('hall', '0');
            $session->set('fifty', '0');
            $session->set('active', 'yes');
        }

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if ((isset($post['answer'])) && (isset($post['hint-hall'])) && (isset($post['hint-call'])) && (isset($post['hint-fifty']))) {
                if (intval($post['hint-hall']) == 1) {
                    $session->set('hall', '1');
                }

                if (intval($post['hint-call']) == 1) {
                    $session->set('call', '1');
                }

                if (intval($post['hint-fifty']) == 1) {
                    $session->set('fifty', '1');
                }

                if (Yii::$app->user->id) {
                    $session->set('user', Yii::$app->user->id);
                }

                if (mb_strlen($post['answer']) != 1) {
                    die();
                    // TODO logger
                }

                $tmp_qid = $this->questionService->fetchAnswer($session->get('qid'));
                if (mb_strtoupper($post['answer']) == mb_strtoupper($tmp_qid)) {
                    $tmp_level = (int)$session->get('level');
                    $tmp_level++;
                    if ($tmp_level > 15) {
                        return $this->redirect(['win']);
                    } else {
                        $session->set('level', $tmp_level);
                    }
                } else {
                    return $this->redirect(['loose']);
                }
            }
        }

        if (intval($session->get('hall')) == 1) {
            $hall = 'used';
        } else {
            $hall = '';
        }
        if (intval($session->get('call')) == 1) {
            $call = 'used';
        } else {
            $call = '';
        }
        if (intval($session->get('fifty')) == 1) {
            $fifty = 'used';
        } else {
            $fifty = '';
        }

        $level = intval($session->get('level'));
        $this->questionService->setLevel($level);
        $question = $this->questionService->getQuestionDTO();
        $question_helper = $this->questionService->getGameHelperDTO();
        $session->set('qid', $this->questionService->getQid());
        $statistic = $this->statisticService->fetchStatistic();

        return $this->render(
            'index',
            [
                'registrationForm' => $registrationForm,
                'login' => $loginForm,
                'changePassword' => $changePasswordForm,
                'question_helper' => $question_helper,
                'question' => $question,
                'hall' => $hall,
                'call' => $call,
                'fifty' => $fifty,
                'statistic' => $statistic,
            ]
        );
    }

    /**
     * @return string
     */
    public function actionLoose()
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->id) {
            $session->set('user', Yii::$app->user->id);
        }
        $model = new Statistic();
        $session->set('active', 'no');
        $level = $session->get('level');
        if ($level == 1) {
            $level = 0;
        } elseif (($level == 5) || ($level == 10)) {
            $level = $this->gameMatch[$level];
        } else {
            $level = $this->gameMatch[($level - 1)];
        }

        $uid = $session->get('user');
        if ($uid) {
            $user = $this->statisticService->getUName($uid);
        } else {
            $user = '';
        }
        $saved = $session->get('saved');
        if (($uid) && (!$saved)) {
            $this->statisticService->publishResult($uid, $level);
            $session->set('saved', 'true');
        }

        $this->layout = false;
        $this->getView()->title = 'Взять деньги';
        $registrationForm = new RegistrationForm();
        $loginForm = new LoginForm();
        $changePasswordForm = new ChangePasswordForm();
        $statistic = $this->statisticService->fetchStatistic();

        if ($registrationForm->load(Yii::$app->request->post()) && $registrationForm->validate()) {
            if ($this->userService->createUser($registrationForm)) {
                return $this->redirect(['loose']);
                    //$this->goHome();
            }
        }

        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->validate()) {
            if ($this->userService->loginUser($loginForm)) {
                return $this->redirect(['loose']);
                    //$this->goHome();
            }
        }
        if ($changePasswordForm->load(Yii::$app->request->post()) && $changePasswordForm->validate()) {
            $this->userService->changePasswordUser($changePasswordForm);
        }

        return $this->render(
            'loose',
            [
                'user' => $user,
                'level' => $level,
                'registrationForm' => $registrationForm,
                'login' => $loginForm,
                'changePassword' => $changePasswordForm,
                'statistic' => $statistic
            ]
        );
    }

    /**
     * This action logout user
     * @return \yii\web\Response
     */
    public function actionExit()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $session->destroy();
        return $this->goHome();
    }

    /**
     * @return \yii\web\Response
     */
    public function actionNewgame()
    {
        $session = Yii::$app->session;
        $session->destroy();
        return $this->redirect('index');
    }

    /**
     * @return string
     */
    public function actionGetmoney()
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->id) {
            $session->set('user', Yii::$app->user->id);
        }
        $model = new Statistic();
        $session->set('active', 'no');
        $level = $session->get('level');
        if ($level == 1) {
            $level = 0;
        } elseif (($level == 5) || ($level == 10)) {
            $level = $this->gameMatch[$level];
        } else {
            $level = $this->gameMatch[($level - 1)];
        }
        $uid = $session->get('user');
        if ($uid) {
            $user = $this->statisticService->getUName($uid);
        } else {
            $user = '';
        }
        $saved = $session->get('saved');
        if (($uid) && (!$saved)) {
            $this->statisticService->publishResult($uid, $level);
            $session->set('saved', 'true');
        }
        $this->layout = false;
        $registrationForm = new RegistrationForm();
        $loginForm = new LoginForm();
        $changePasswordForm = new ChangePasswordForm();
        $this->getView()->title = 'Взять деньги';
        $statistic = $this->statisticService->fetchStatistic();

        if ($registrationForm->load(Yii::$app->request->post()) && $registrationForm->validate()) {
            if ($this->userService->createUser($registrationForm)) {
                return $this->redirect(['getmoney']);
                //$this->goHome();
            }
        }

        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->validate()) {
            if ($this->userService->loginUser($loginForm)) {
                return $this->redirect(['getmoney']);
                //$this->goHome();
            }
        }
        if ($changePasswordForm->load(Yii::$app->request->post()) && $changePasswordForm->validate()) {
            $this->userService->changePasswordUser($changePasswordForm);
        }

        return $this->render(
            'getmoney',
            [
                'user' => $user,
                'level' => $level,
                'registrationForm' => $registrationForm,
                'login' => $loginForm,
                'changePassword' => $changePasswordForm,
                'statistic' => $statistic
            ]
        );
    }

    /**
     * @return string
     */
    public function actionWin()
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->id) {
            $session->set('user', Yii::$app->user->id);
        }
        $model = new Statistic();
        $session->set('active', 'no');
        $level = $session->get('level');
        if ($level == 1) {
            $level = 0;
        } elseif (($level == 5) || ($level == 10)) {
            $level = $this->gameMatch[$level];
        } else {
            $level = $this->gameMatch[($level - 1)];
        }
        $uid = $session->get('user');
        if ($uid) {
            $user = $this->statisticService->getUName($uid);
        } else {
            $user = '';
        }
        $saved = $session->get('saved');
        if (($uid) && (!$saved)) {
            $this->statisticService->publishResult($uid, $level);
            $session->set('saved', 'true');
        }
        $this->layout = false;
        $registrationForm = new RegistrationForm();
        $loginForm = new LoginForm();
        $changePasswordForm = new ChangePasswordForm();
        $this->getView()->title = 'Вы виграли 1 миллион';
        $statistic = $this->statisticService->fetchStatistic();

        if ($registrationForm->load(Yii::$app->request->post()) && $registrationForm->validate()) {
            if ($this->userService->createUser($registrationForm)) {
                return $this->redirect(['win']);
                //$this->goHome();
            }
        }

        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->validate()) {
            if ($this->userService->loginUser($loginForm)) {
                return $this->redirect(['win']);
                //$this->goHome();
            }
        }
        if ($changePasswordForm->load(Yii::$app->request->post()) && $changePasswordForm->validate()) {
            $this->userService->changePasswordUser($changePasswordForm);
        }

        return $this->render(
            'win',
            [
                'user' => $user,
                'registrationForm' => $registrationForm,
                'login' => $loginForm,
                'changePassword' => $changePasswordForm,
                'statistic' => $statistic
            ]
        );
    }

}
