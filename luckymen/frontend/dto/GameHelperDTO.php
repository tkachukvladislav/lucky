<?php

namespace frontend\dto;

/**
 * Class GameHelperDTO
 * @package frontend\dto
 */
class GameHelperDTO
{
    /**
     * @var string
     */
    private string $usedCall;
    /**
     * @var string
     */
    private string $usedHall;
    /**
     * @var string
     */
    private string $usedFifty;
    /**
     * @var string
     */
    private string $helpopts;
    /**
     * @var int
     */
    private int $step;
    /**
     * @var string
     */
    private string $callopts;
    /**
     * @var string
     */
    private string $fiftyopts;

    /**
     * GameHelperDTO constructor.
     * @param string $helpopts
     * @param int $step
     * @param string $callopts
     * @param string $fiftyopts
     */
    public function __construct($helpopts = 'A', $step = 1, $callopts = 'я не знаю :(', $fiftyopts = '', $usedCall='0',$usedFifty='0', $usedHall='0')
    {
        $this->helpopts = $helpopts;
        $this->step = $step;
        $this->callopts = $callopts;
        $this->fiftyopts = $fiftyopts;
        $this->usedCall = $usedCall;
        $this->usedFifty = $usedFifty;
        $this->usedHall = $usedHall;
    }

    /**
     * @return string
     */
    public function getUsedFifty(): string
    {
        return $this->usedFifty;
    }

    /**
     * @return string
     */
    public function getUsedCall(): string
    {
        return $this->usedCall;
    }

    /**
     * @return string
     */
    public function getUsedHall(): string
    {
        return $this->usedHall;
    }

    /**
     * @return string
     */
    public function getCallopts(): string
    {
        return $this->callopts;
    }

    /**
     * @return string
     */
    public function getFiftyopts(): string
    {
        return $this->fiftyopts;
    }

    /**
     * @return string
     */
    public function getHelpopts(): string
    {
        return $this->helpopts;
    }

    /**
     * @return int
     */
    public function getStep(): int
    {
        return $this->step;
    }
}
