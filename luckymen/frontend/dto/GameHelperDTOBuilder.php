<?php

namespace frontend\dto;

/**
 * Class GameHelperDTOBuilder
 * @package frontend\dto
 */
class GameHelperDTOBuilder
{
    /**
     * @var string
     */
    public string $helpopts;
    /**
     * @var int
     */
    public int $step;
    /**
     * @var string
     */
    public string $callopts;
    /**
     * @var string
     */
    public string $fiftyopts;

    /**
     * @return GameHelperDTO
     */
    public function build(): GameHelperDTO
    {
        return new GameHelperDTO($this->helpopts, $this->step, $this->callopts, $this->fiftyopts);
    }
}
