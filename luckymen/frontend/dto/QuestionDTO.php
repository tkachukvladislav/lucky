<?php

namespace frontend\dto;

/**
 * Class QuestionDTO
 * @package frontend\dto
 */
class QuestionDTO
{
    /**
     * @var string
     */
    private string $a;
    /**
     * @var string
     */
    private string $b;
    /**
     * @var string
     */
    private string $c;
    /**
     * @var string
     */
    private string $d;
    /**
     * @var string
     */
    private string $question;

    /**
     * @return string
     */
    public function getA(): string
    {
        return $this->a;
    }

    /**
     * @return string
     */
    public function getB(): string
    {
        return $this->b;
    }

    /**
     * @return string
     */
    public function getC(): string
    {
        return $this->c;
    }

    /**
     * @return string
     */
    public function getD(): string
    {
        return $this->d;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * QuestionDTO constructor.
     * @param string $a
     * @param string $b
     * @param string $c
     * @param string $d
     * @param string $question
     */
    public function __construct(
        string $a = 'A',
        string $b = 'B',
        string $c = 'C',
        string $d = 'D',
        string $question = 'Нет вопроса :('
    ) {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->d = $d;
        $this->question = $question;
    }
}
