<?php

namespace frontend\dto;

/**
 * Class QuestionDTOBuilder
 * @package frontend\dto
 */
class QuestionDTOBuilder
{
    /**
     * @var string
     */
    public string $a;
    /**
     * @var string
     */
    public string $b;
    /**
     * @var string
     */
    public string $c;
    /**
     * @var string
     */
    public string $d;
    /**
     * @var string
     */
    public string $question;

    /**
     * @return QuestionDTO
     */
    public function build(): QuestionDTO
    {
        return new QuestionDTO($this->a, $this->b, $this->c, $this->d, $this->question);
    }
}
