<?php

namespace frontend\form;

use yii\base\Model;

/**
 * Class ChangePasswordForm
 * @package frontend\form
 */
class ChangePasswordForm extends Model
{
    /**
     * @var
     */
    public  $password_hash;
    /**
     * @var
     */
    public  $confrimPassword;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['password_hash','confrimPassword'],'required'],
            ['password_hash','trim'],
            ['confrimPassword','trim'],
            [['password_hash'], 'validatePassword'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'password_hash' => 'Your password',
            'passwordConfrim' => 'Your confrim passwrod',
        ];
    }

    /**
     *
     */
    public function validatePassword()
    {
        if ($this->password_hash !== $this->confrimPassword) {

            $this->addError('password_hash', 'Ваши пароли не совпадают');
        }
    }
}