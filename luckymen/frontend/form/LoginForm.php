<?php

namespace frontend\form;

use yii\base\Model;

/**
 * Class LoginForm
 * @package frontend\form
 */
class LoginForm extends Model
{
    /**
     * @var
     */
    public $login;
    /**
     * @var
     */
    public $password_hash;

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Your login',
            'password_hash' => 'Your password',
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['login','password_hash'],'required']
        ];
    }
}