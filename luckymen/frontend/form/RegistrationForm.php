<?php

namespace frontend\form;

use yii\base\Model;

/**
 * Class RegistrationForm
 * @package frontend\form
 */
class RegistrationForm extends Model
{
    /**
     * @var
     */
    public $login;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $password_hash;
    /**
     * @var
     */
    public $passwordConfrim;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['login', 'email', 'password_hash', 'passwordConfrim'], 'required'],
            ['login', 'trim'],
            ['email', 'email',],
            ['email', 'trim'],
            [['password_hash'], 'validatePassword'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Your login',
            'email' => 'Your email address',
            'password_hash' => 'Your password',
            'passwordConfrim' => 'Your confrim passwrod',
        ];
    }

    /**
     *Validate password
     */
    public function validatePassword()
    {
        if ($this->password_hash !== $this->passwordConfrim) {

            $this->addError('password_hash', 'Your passwords do not match');
        }
    }
}