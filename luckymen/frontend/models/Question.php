<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property string $id
 * @property string $question
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $answer
 * @property int $level
 * @property string|null $create_date
 * @property string|null $update_date
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'question', 'a', 'b', 'c', 'd', 'answer', 'level'], 'required'],
            [['id'], 'string'],
            [['level'], 'default', 'value' => null],
            [['level'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['question', 'a', 'b', 'c', 'd', 'answer'], 'string', 'max' => 255],
            [['question'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'a' => 'A',
            'b' => 'B',
            'c' => 'C',
            'd' => 'D',
            'answer' => 'Answer',
            'level' => 'Level',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
