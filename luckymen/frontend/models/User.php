<?php

namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $login
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 *
 * @property Statistic $statistic
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     *
     */
    const STATUS_DELETED = 0;
    /**
     *
     */
    const STATUS_INACTIVE = 9;
    /**
     *
     */
    const STATUS_ACTIVE = 10;

    /**
     * This table User
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * This add and update Date in table
     */
    public function behaviors()
    {

        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * Validation attribute
     */
    public function rules()
    {
        return [
            [['id', 'login', 'auth_key', 'password_hash', 'email'], 'required'],
            [['id'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['login', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['login'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['id'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]]
        ];
    }


    /**
     * Find identity User
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * findIdentityByAccessToken
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $login
     * @return static|null
     */
    public static function findByUserLogin($login)
    {
        return static::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByUserEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * get Id User
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Get AuthKey Use
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validator user AuthKey
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generate Password Argon2
     */
    public function setPassword($password)
    {
        $this->password_hash = $password;
    }

    /**
     *Generate Password ARGON2
     */
    public function generatePassword()
    {
        $this->password_hash = password_hash($this->password_hash, PASSWORD_ARGON2I);
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword(string $password): bool
    {
        return password_verify($password, $this->password_hash);
    }

    /**
     * Gets query for [[Statistic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatistic()
    {
        return $this->hasOne(Statistic::class, ['id' => 'id']);
    }

}
