<?php

namespace frontend\repositories;

use frontend\models\Question;
use yii\db\Expression;

/**
 * Class QuestionRepositories
 * @package frontend\repositories
 */
class QuestionRepositories
{
    /**
     * @param int $level
     * @return Question
     */
    public function fetchQuestion(int $level): Question
    {
        //Tiny randomizer for query
        return Question::find()->where(['level' => $level])->orderBy(new Expression('random()'))->one();
    }

    /**
     * @param string $qid
     * @return Question
     */
    public function getAnswerById(string $qid): Question
    {
        //todo uuid
        return Question::find()->where(['id' => $qid])->one();
    }

}
