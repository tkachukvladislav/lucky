<?php

namespace frontend\repositories;

use frontend\models\Statistic;
use frontend\models\User;
use Yii;

/**
 * Class StatisticRepositories
 * @package frontend\repositories
 */
class StatisticRepositories
{
    /**
     * @return array
     */
    public function fetchStatistic(): array
    {
        $prepared = [];
        $test = Statistic::find()->limit(10)->orderBy(['result' => SORT_DESC])->all();
        foreach ($test as $ucheck) {
            $ucheck->id;
            $user = User::find()->where(['id' => $ucheck->id])->one();
            $prepared[] = ['user' => $user->login, 'result' => $ucheck->result];
        }
        return $prepared;
    }

    /**
     * @param string $uid
     * @param string $result
     * @throws \Throwable
     */
    public function publishResult(string $uid, string $result)
    {
        //todo uuid
        $model = new Statistic();
        $model->id = $uid;
        $model->result = $result;
        $model->date_game =Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $model->insert();
    }

    /**
     * @param string $uid
     * @return string
     */
    public function getUName(string $uid): string
    {
        //todo uuid
        $user = User::find()->where(['id' => $uid])->one();
        $user = $user->login;
        return $user;
    }
}
