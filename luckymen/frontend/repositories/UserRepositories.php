<?php

namespace frontend\repositories;

use frontend\models\User;
use Yii;

/**
 * Class UserRepositories
 * @package frontend\repositories
 */
class UserRepositories
{

    /**
     * @param User $user
     * @return bool
     */
    public function save(User $user): bool
    {
        if (!$user->save()) {
            Yii::error($user->getErrors() . '');
            return false;
        }

        return true;
    }

    /**
     * @param string $login
     * @return User|null
     */
    public function findByUserLogin(string $login): ?User
    {
        return User::findByUserLogin($login);
    }

    /**
     * @param string $id
     * @return User|null
     */
    public function findByUserId(string $id): ?User
    {
        return User::findOne($id);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findByUserEmail(string $email): ?User
    {
        return User::findByUserEmail($email);
    }

}