<?php

namespace frontend\service;

use frontend\dto\GameHelperDTOBuilder;
use frontend\dto\QuestionDTOBuilder;
use frontend\dto\QuestionDTO;
use frontend\models\Question;
use frontend\repositories\QuestionRepositories;
use phpDocumentor\Reflection\Type;
use frontend\dto\GameHelperDTO;
use yii\di\ServiceLocator;
use yii\web\Controller;

class QuestionService
{
    /**
     * @var QuestionRepositories
     */
    private QuestionRepositories $questionRepositories;
    private Question $question;
    private int $prob;
    private array $matcher = [
        1 => 'A',
        2 => 'B',
        3 => 'C',
        4 => 'D',
    ];
    private array $fifty_fifty_a = ['A', 'C'];
    private array $fifty_fifty_b = ['B', 'D'];
    private string $qid;
    private int $level;

    /**
     * @return string
     */
    public function getQid(): string
    {
        return $this->qid;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    public function __construct(QuestionRepositories $questionRepositories)
    {
        $this->questionRepositories = $questionRepositories;
        $this->prob = random_int(1, 100);
    }

    public function fetchAnswer(string $qid): string
    {
        //Todo uuid
        $tmp = $this->questionRepositories->getAnswerById($qid);
        return $tmp->answer;
    }

    public function setLevel(int $level): QuestionService
    {
        $this->level = $level;
        $this->question = $this->questionRepositories->fetchQuestion($this->level);
        $this->qid = $this->question->id;
        return $this;
    }

    public function getQuestionDTO(): QuestionDTO
    {
        $builder = new QuestionDTOBuilder();
        $builder->question = $this->question->question;
        $builder->a = $this->question->a;
        $builder->b = $this->question->b;
        $builder->c = $this->question->c;
        $builder->d = $this->question->d;

        return $builder->build();
    }

    public function getGameHelperDTO(): GameHelperDTO
    {
        $builder = new GameHelperDTOBuilder();
        $builder->step = $this->level;

        //Fifty fifty section
        if (in_array(strtoupper($this->question->answer), $this->fifty_fifty_a)) {
            $builder->fiftyopts = ($this->fifty_fifty_b[0] . ', ' . $this->fifty_fifty_b[1]);
        } else {
            $builder->fiftyopts = ($this->fifty_fifty_a[0] . ', ' . $this->fifty_fifty_a[1]);
        }

        //Call help define section
        if ($this->prob > 68) {
            $builder->callopts = mb_strtoupper($this->question->answer);
        } else {
            $rnd = random_int(1, 4);
            $builder->callopts = mb_strtoupper($this->matcher[$rnd]);
        }

        //Studio help method
        $helpopts = ['A' => 0, 'B' => 0, 'C' => 0, 'D' => 0];
        $helpopts['A'] = random_int(1, 30);
        $helpopts['B'] = random_int(1, 30);
        $helpopts['C'] = random_int(1, 30);
        $helpopts['D'] = random_int(1, 30);
        if ($this->prob > 50) {
            $index = mb_strtoupper(strval($this->question->answer));
            $tmp = $helpopts[$index];
            $helpopts[$index] =  $tmp + random_int(30, 60);
        } else {
            $helpopts[$this->matcher[random_int(1, 4)]] = $helpopts[$this->matcher[random_int(1, 4)]] +random_int(
                    0,
                    30
                );
        }
        $builder->helpopts = '' . $helpopts['A'] . ', ' . $helpopts['B'] . ', ' . $helpopts['C'] . ', ' . $helpopts['D'];
        return $builder->build();
    }
}
