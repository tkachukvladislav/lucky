<?php

namespace frontend\service;

use frontend\repositories\StatisticRepositories;

/**
 * Class StatisticService
 * @package frontend\service
 */
class StatisticService
{
    /**
     * @var StatisticRepositories
     */
    private StatisticRepositories $statisticRepositories;

    /**
     * StatisticService constructor.
     * @param StatisticRepositories $statisticRepositories
     */
    public function __construct(StatisticRepositories $statisticRepositories)
    {
        $this->statisticRepositories = $statisticRepositories;
    }

    /**
     * @return array
     */
    public function fetchStatistic():array
    {
        return $this->statisticRepositories->fetchStatistic();
    }

    /**
     * @param string $uid
     * @return string
     */
    public function getUName(string $uid):string
    {
        return $this->statisticRepositories->getUName($uid);
    }

    /**
     * @param string $uid
     * @param string $result
     */
    public function publishResult(string $uid, string $result)
    {
        $this->statisticRepositories->publishResult($uid,  $result);
    }
}
