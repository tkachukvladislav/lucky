<?php

namespace frontend\service;

use frontend\form\ChangePasswordForm;
use frontend\form\LoginForm;
use frontend\form\RegistrationForm;
use frontend\models\User;
use frontend\repositories\UserRepositories;
use Ramsey\Uuid\Uuid;
use Yii;


/**
 * Class UserService
 * @package frontend\service
 */
class UserService
{
    /**
     * @var UserRepositories
     */
    private $userRepositories;

    /**
     * UserService constructor.
     *
     * @param UserRepositories $userRepositories
     */
    public function __construct(UserRepositories $userRepositories)
    {
        $this->userRepositories = $userRepositories;
    }

    /**
     * @param RegistrationForm $form
     * @throws \Exception
     */
    public function createUser(RegistrationForm $form): bool
    {
        $user = new User();
        $user->load($form->getAttributes(), '');
        $user->id = Uuid::uuid4()->toString();
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->generatePassword();

        if (!$this->userRepositories->findByUserLogin($user->login) && !$this->userRepositories->findByUserEmail($user->email)) {
            if ($this->userRepositories->save($user)) {

                return true;
            }
        }

        $form->addErrors($user->getErrors());

        return false;
    }

    /**
     * This
     * @param LoginForm $loginForm
     * @return bool
     */
    public function loginUser(LoginForm $loginForm): bool
    {
        $user = null;
        if ($this->userRepositories->findByUserLogin($loginForm->login)) {

            $user = $this->userRepositories->findByUserLogin($loginForm->login);

            if ($user->validatePassword($loginForm->password_hash) && Yii::$app->user->login($user)) {

                return true;
            }
        }

        $loginForm->addError('password_hash', 'Incorrect login or password');

        return false;
    }

    /**
     * @param ChangePasswordForm $changePasswordForm
     * @return bool
     */
    public function changePasswordUser(ChangePasswordForm $changePasswordForm): bool
    {
        $user = $this->userRepositories->findByUserId(Yii::$app->user->getId());
        $user->setPassword($changePasswordForm->password_hash);
        $user->generatePassword();

        if ($this->userRepositories->save($user)) {

            return true;
        }
        $changePasswordForm->addError();

        return false;
    }
}
