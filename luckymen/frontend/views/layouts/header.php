<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<header class="main-header">
    <?php


    if (Yii::$app->user->isGuest):?>
    <button type="button" class="register-btn" id="loginBtn"
            onclick="showModal(document.getElementById('modalLogin'));">Логин
    </button>
    <button type="button" class="register-btn" id="registerBtn"
            onclick="showModal(document.getElementById('modalRegister'));">Регистрация
    </button>
        <button type="button" class="register-btn" id="statisticBtn"
                onclick="showModal(document.getElementById('modalStatistic'));">Ститистика
        </button>
    <?php else : ?>
        <?= Html::a('Logout', Url::to('exit'),['class'=>'register-btn']) ?>
        <button type="button" class="register-btn" id="registerBtn"
                onclick="showModal(document.getElementById('modalChangePassword'));">Change-Password
        </button>
        <?= Html::button('Statistic',['class'=>' btn register-btn']) ?>
    <?php endif ?>
</header>

