<?php
/* @var  $login \frontend\form\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<div class="modal" id="modalLogin">
    <div class="modal-body">
        <span class="modal-close">x</span>
        <h2 class="login-title">Sign In</h2>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'login-form']]) ?>

        <?= $form->field($login, 'login')
            ->textInput(['placeholder' => 'Login', 'class' => 'login-input'])
            ->label(false) ?>

        <?= $form->field($login, 'password_hash')
            ->passwordInput(['placeholder' => 'Password', 'class' => 'login-input'])
            ->label(false) ?>

        <?= Html::submitButton('Login',['class'=>'modal-btn']) ?>

        <?php ActiveForm::end() ?>

    </div>
</div>
