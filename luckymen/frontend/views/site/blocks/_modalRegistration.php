<?php
/* @var $registrationForm \frontend\form\RegistrationForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="modal" id="modalRegister">
    <span class="modal-close">x</span>
    <div class="modal-body">
        <h2 class="login-title">Register</h2>
        <?php $form = ActiveForm::begin(['options'=>['class'=>'login-form']]) ?>
        <?= $form->field($registrationForm, 'login')
            ->textInput(['placeholder'=>'Login','class'=>'login-input'])
            ->label(false)?>

        <?= $form->field($registrationForm, 'email')
            ->textInput(['placeholder'=>'Email','class'=>'login-input'])
            ->label(false) ?>

        <?= $form->field($registrationForm, 'password_hash')
            ->passwordInput(['placeholder'=>'Enter password','class'=>'login-input'])
            ->label(false) ?>

        <?= $form->field($registrationForm, 'passwordConfrim')
            ->passwordInput(['placeholder'=>'Confirm your password','class'=>'login-input'])
            ->label(false) ?>

        <?= Html::submitButton('Registr',['class'=>'modal-btn']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>