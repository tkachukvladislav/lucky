<?php
/* @var $question_helper frontend\dto\GameHelperDTO */
/* @var $question frontend\dto\QuestionDTO */
?>
<form action="" method="POST" id="questionsForm">
    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
    <input type="text" hidden value="0" name="hint-hall" id='report-hall' data-answer="<?php echo $question_helper->getHelpopts(); ?>">
    <input type="text" hidden value="0" name="hint-fifty" id='report-fifty' data-answer="<?php echo $question_helper->getFiftyopts(); ?>"">
    <input type="text" hidden value="0" name="hint-call" id='report-call' >
    <input type="text" hidden value="Z" name="answer" id="Answer">

    <section class="main-section">
        <div class="hints-loose">
            <div class="hint hall <?= $hall;?>" id="hintHall"></div>
            <div class="hint fifty <?= $fifty;?>" id="hintFifty"></div>
            <div class="hint call <?= $call;?>" id="hintCall"></div>
        </div>
        <div class="question main-theme-styles">
            <?php echo $question->getQuestion() ?>
        </div>
        <div class="answer-bg">
            <button type="submit" class="answer main-theme-styles" value="A" id="buttonA" onclick="$('#Answer').val('A');">A: <?php echo $question->getA(); ?></button>
            <button type="submit" class="answer main-theme-styles" value="B" id="buttonB" onclick="$('#Answer').val('B')">B: <?php echo $question->getB(); ?></button>
        </div>
        <div class="answer-bg">
            <button type="submit" class="answer main-theme-styles" value="C" id="buttonC" onclick="$('#Answer').val('C')">C: <?php echo $question->getC(); ?></button>
            <button type="submit" class="answer main-theme-styles" value="D" id="buttonD" onclick="$('#Answer').val('D')">D: <?php echo $question->getD(); ?></button>
        </div>
    </section>
</form>