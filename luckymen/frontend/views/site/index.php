<?php
/* @var $this yii\web\View */

/* @var $model \frontend\model\Question */
/* @var $registrationForm frontend\form\RegistrationForm */
/* @var $login frontend\form\LoginForm */
/* @var $changePassword frontend\form\ChangePasswordForm */
/* @var $question frontend\dto\QuestionDTO */
/* @var $question_helper frontend\dto\GameHelperDTO */

use frontend\assets\MillionAsset;
use yii\helpers\Html;


MillionAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?= Html::csrfMetaTags() ?>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <title><?php echo $this->title ?></title>
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    <body>
    <main>
        <?php echo $this->render('/layouts/header') ?>
        <?php echo $this->render('blocks/_questionsForm', ['question_helper' => $question_helper, 'question' => $question,'hall' => $hall, 'call' => $call, 'fifty' => $fifty]) ?>
        <section>
        <ul class="answers-list">
            <li class="fire" id="tab15"><span class="num">15</span><span class="money"><b>1 000 000</b></span></li>
            <li id="tab14"><span class="num">14</span><span class="money"><b>500 000</b></span></li>
            <li id="tab13"><span class="num">13</span><span class="money"><b>250 000</b></span></li>
            <li id="tab12"><span class="num">12</span><span class="money"><b>125 000</b></span></li>
            <li id="tab11"><span class="num">11</span><span class="money"><b>64 000</b></span></li>
            <li class="fire" id="tab10"><span class="num">10</span><span class="money"><b>32 000</b></span></li>
            <li id="tab9"><span class="num" id="tab9">9</span><span class="money"><b>16 000</b></span></li>
            <li id="tab8"><span class="num" id="tab8">8</span><span class="money"><b>8 000</b></span></li>
            <li id="tab7"><span class="num">7</span><span class="money"><b>4 000</b></span></li>
            <li id="tab6"><span class="num" id="tab6">6</span><span class="money"><b>2 000</b></span></li>
            <li class="fire" id="tab5"> <span class="num">5</span><span class="money"><b>1 000</b></span></li>
            <li id="tab4"><span class="num">4</span><span class="money"><b>500</b></span></li>
            <li id="tab3"><span class="num">3</span><span class="money"><b>300</b></span></li>
            <!-- Класс dot должен быть на том этапе, который уже был пройден -->
            <li id="tab2"><span class="num">2</span><span class="money"><b>200</b></span></li>
            <li id="tab1"><span class="num">1</span><span class="money"><b>100</b></span></li>
            <span class="hide" id="tabstep"><?php echo $question_helper->getStep(); ?></span>
            <span class="hide" id="tabUsedCall"><?php echo $question_helper->getUsedCall(); ?></span>
            <span class="hide" id="tabUsedFifty"><?php echo $question_helper->getUsedFifty(); ?></span>
            <span class="hide " id="tabUsedHall"><?php echo $question_helper->getUsedHall(); ?></span>
        </ul>

    </main>


    <!-- Чтоб посмотреть любое модальное окно, к соответствующему модальному окну добавить класс show.
    По клику на кнопку или на область вне модального окна- класс будет удален и модалка исчезнет -->

    <!--  - модальное окно - для помощи друга -->
    <?php echo $this->render('blocks/_modalFriend', ['question_helper' => $question_helper]) ?>

    <!-- модально окно, когда выиграл миллион -->
    <div class="modal" id="win">
        <div class="modal-body">
            <img src="million/img/congratulations.png" alt="congratulations icon" class="modal-img">
            <p class="modal-message">Поздравляю! Вы выиграли 10000000</p>
            <button type="button" class="modal-btn">Спасибо</button>
        </div>

    </div>

    <!-- модальное окно когда закончил игру, но не выиграл миллион -->
    <?php echo $this->render('blocks/_modalLoose') ?>

    <!-- модальное окно по достижению несгораемой суммы -->
    <form action="getmoney" method="POST" id="getmoney">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>"
               value="<?= Yii::$app->request->getCsrfToken(); ?>"/>
        <input type="text" hidden value="true" name="getmoney">
    </form>

    <div class="modal" id="firesum">
        <div class="modal-body">
            <img src="million/img/congratulations.png" alt="congratulations icon" class="modal-img">
            <p class="modal-message">Поздравляю! Вы достигли несгораемой суммы! Берем деньги или играем дальше?</p>
            <button type="button" class="modal-btn">Продолжить играть</button>
            <button type="button" class="modal-btn"><a href="#" onclick="document.getElementById('getmoney').submit();">
                    Взять деньги и уйти </a></button>
        </div>
        </form>
    </div>

    <!-- модальное окно подсказка зала-->
    <?php echo $this->render('blocks/_modalHallAnswer') ?>

    <?php echo $this->render('blocks/_modalStatistic',['statistic' => $statistic]) ?>

    <!-- модальное окно регистрации-->
    <?php echo $this->render('blocks/_modalRegistration', ['registrationForm' => $registrationForm]) ?>
    <!-- модальное окно логина-->
    <?php echo $this->render('blocks/_modalLogin', ['login' => $login]) ?>
    <!-- модальное окно смены пароля-->
    <?php echo $this->render('blocks/_modalChangePassword', ['changePassword' => $changePassword]) ?>
    <?= $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>