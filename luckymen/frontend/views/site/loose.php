<?php
/* @var $this yii\web\View */

use frontend\assets\MillionAsset;
use yii\helpers\Html;

MillionAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags() ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <title><?php echo $this->title ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body>
<main>
    <?php echo $this->render('/layouts/header') ?>


    <div class="game-loose">
            <h5>Вы проиграли</h5><br>
            <h5>Ваш резултат: <?= $level;?></h5><br>
            <h5><?= $user;?></h5><br>
        <h5> Хочеш сохранить результат ? </h5><br>
              зарегистрируйся <br><br>

        <a href="/newgame">Новая игра</a>
    </div>
</main>

<?php echo $this->render('blocks/_modalStatistic',['statistic' => $statistic]) ?>
<?php echo $this->render('blocks/_modalRegistration', ['registrationForm' => $registrationForm]) ?>
<!-- модальное окно логина-->
<?php echo $this->render('blocks/_modalLogin', ['login' => $login]) ?>
<!-- модальное окно смены пароля-->
<?php echo $this->render('blocks/_modalChangePassword', ['changePassword' => $changePassword]) ?>


<?= $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
