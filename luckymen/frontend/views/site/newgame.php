<?php
/* @var $this yii\web\View */

use frontend\assets\MillionAsset;
use yii\helpers\Html;

MillionAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags() ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <title><?php echo $this->title ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body>
<main>
    <?php echo $this->render('/layouts/header') ?>
</main>

    <div class='modal show'>
        <div class="modal-body">
            <h5>Новая игра</h5>
        </div>
    </div>

<?= $this->endBody() ?>
<?php $this->registerJs('
    $(document).ready(function () {
        window.setTimeout(function () {
            location.href = "/";
        }, 1000);
    });
'); ?>

</body>

</html>

<?php $this->endPage() ?>
